import request from '@/utils/request'
import { JSEncrypt } from 'jsencrypt'

/**
 * 获取加密后的登录入参
 * @param loginParams
 * @returns
 */
export async function authLoginParams(loginParams: {
  username: string
  password: string
  code: any
  uuid: any
}) {
  const keysInfo = await issuePublicKey()
  const jse = new JSEncrypt()
  let account = ''
  let pwd = ''
  if (keysInfo) {
    jse.setPublicKey(keysInfo.data.publicKey) // 配置公钥
    account = String(jse.encrypt(loginParams.username.trim())) // 加密账号
    pwd = String(jse.encrypt(loginParams.password)) // 加密密码
  }
  return {
    scope: 'read',
    grant_type: 'encryption',
    username: account,
    password: pwd,
    rsaKey: keysInfo.data.rsaKey,
    code: loginParams.code,
    uuid: loginParams.uuid
  }
}

// 获取公钥信息
export function issuePublicKey(data = {}) {
  return request({
    url: import.meta.env.VITE_BASEPROXY + '/auth/token/issuePublicKey',
    method: 'post',
    data: data
  })
}

//登录

export function loginApi(data: any) {
  return request({
    url: import.meta.env.VITE_BASEPROXY + '/frontend-api/auth/login',
    method: 'post',
    data,
    // noToken: true,
    headers: {
      isToken: false,
      repeatSubmit: false
    }
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: import.meta.env.VITE_BASEPROXY + '/frontend-api/system/user/getInfo',
    // needAll: true,
    method: 'get'
    // params: { token: getToken() }
  })
}

//获取权限菜单
export function getSysMenu(params = { productCode: 'regulatory-platform' }) {
  return request({
    url: import.meta.env.VITE_APIPROXY + '/menu/getSysMenu',
    method: 'get',
    params
  })
}

export function logout() {
  return request({
    url: import.meta.env.VITE_BASEPROXY + '/frontend-api/auth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: import.meta.env.VITE_APIPROXY + '/code',
    headers: {
      isToken: false
    },
    // needAll: true,
    method: 'get'
  })
}

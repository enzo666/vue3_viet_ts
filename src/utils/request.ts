import axios from 'axios'
import { getToken } from '@/utils/auth'
import cache from '@/plugins/cache'
import { tansParams } from '@/utils/ruoyi'
import errorCode from '@/utils/errorCode'
import { ElMessage, ElMessageBox, ElNotification } from 'element-plus'
import type { AxiosResponse } from 'axios'
// 是否显示重新登录
export let isRelogin = {
  show: false
}
const router = useRouter()

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// 创建axios实例
const request = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: '',
  // baseURL: import.meta.env.VITE_APIPROXY,
  // 超时
  timeout: 150000
})
// request拦截器
request.interceptors.request.use(
  (config) => {
    // 是否需要设置 token
    const isToken = (config.headers || {}).isToken === false
    // 是否需要防止数据重复提交
    const isRepeatSubmit = (config.headers || {}).repeatSubmit === false
    if (getToken() && !isToken) {
      config.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    }

    // get请求映射params参数
    if (!config.params) {
      config.params = {}
    }
    if ((config.method === 'get' || config.method === 'delete') && config.params) {
      if (typeof config.params == 'string') {
        let url = config.url + `?projectId=` + config.params
        url = url.slice(0, -1)
        config.params = {}
        config.url = url
      } else {
        let url = config.url + '?' + tansParams(config.params)
        url = url.slice(0, -1)
        config.params = {}
        config.url = url
      }
    }
    if (!isRepeatSubmit && (config.method === 'post' || config.method === 'put')) {
      if (!config.data) {
        config.data = {}
      }
      const requestObj = {
        url: config.url,
        data: typeof config.data === 'object' ? JSON.stringify(config.data) : config.data,
        time: new Date().getTime()
      }
      console.log(config.data)

      const requestSize = Object.keys(JSON.stringify(requestObj)).length // 请求数据大小
      const limitSize = 5 * 1024 * 1024 // 限制存放数据5M
      if (requestSize >= limitSize) {
        console.warn(`[${config.url}]: ` + '请求数据大小超出允许的5M限制，无法进行防重复提交验证。')
        return config
      }
      const sessionObj = cache.session.getJSON('sessionObj')
      if (sessionObj === undefined || sessionObj === null || sessionObj === '') {
        cache.session.setJSON('sessionObj', requestObj)
      } else {
        const s_url = sessionObj.url // 请求地址
        const s_data = sessionObj.data // 请求数据
        const s_time = sessionObj.time // 请求时间
        const interval = 1000 // 间隔时间(ms)，小于此时间视为重复提交
        if (
          s_data === requestObj.data &&
          requestObj.time - s_time < interval &&
          s_url === requestObj.url
        ) {
          const message = '数据正在处理，请勿重复提交'
          console.warn(`[${s_url}]: ` + message)
          return Promise.reject(new Error(message))
        } else {
          cache.session.setJSON('sessionObj', requestObj)
        }
      }
    }

    return config
  },
  (error) => {
    console.log(error)
    Promise.reject(error)
  }
)
// 响应拦截器
request.interceptors.response.use(
  (res: AxiosResponse) => {

    // 未设置状态码则默认成功状态
    const code = res.data.code || 200
    // 获取错误信息
    const msg = errorCode[code] || res.data.msg || errorCode['default']

    // 判断返回格式是否文件流拉起下载逻辑
    if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
      if (res.data.type === 'application/json') {
        const reader = new FileReader()
        reader.onload = () => {
          // TODO 错误处理
          console.log('baocuo')
        }
        reader.readAsText(res.data)
        return null
      } else {
        let fileName = res.headers['content-disposition'].split('=')[1] || '下载文件模板'
        // 后端未返回名字，前端暂时写死文件名
        // console.log(res.data)
        // let fileName = '下载文件模板';
        const url = URL.createObjectURL(res.data)
        const link = document.createElement('a')
        document.body.appendChild(link)
        link.download = decodeURIComponent(fileName)
        link.href = url
        link.click()
        document.body.removeChild(link)
        URL.revokeObjectURL(url)
        return res
      }
    }
    if (code === 401) {
      if (!isRelogin.show) {
        isRelogin.show = true
        ElMessageBox.confirm('登录状态已过期，您可以继续留在该页面，或者重新登录', '系统提示', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            isRelogin.show = false
            // store.dispatch('LogOut').then(() => {
            //   if (process.env.NODE_ENV === 'development') {
            //     router.push('/login')
            //     // location.href = '/';
            //     location.reload()
            //   } else {
            //     location.replace(process.env.VUE_APP_XJ + '/login')
            //   }
            // })
          })
          .catch(() => {
            isRelogin.show = false
          })
      }
      return Promise.reject('无效的会话，或者会话已过期，请重新登录。')
    } else if (code === 500) {
      ElMessage({
        message: msg,
        type: 'error'
      })
      return Promise.reject(new Error(msg))
    } else if (code === 601) {
      ElMessage({
        message: msg,
        type: 'warning'
      })
      return Promise.reject('error')
    } else if (code !== 200) {
      ElNotification.error({
        title: msg
      })
      return Promise.reject('error')
    } else {
      return res.data
    }
  },
  (error) => {
    console.log('err' + error)
    let { message } = error
    if (message == 'Network Error') {
      message = '后端接口连接异常'
    } else if (message.includes('timeout')) {
      message = '系统接口请求超时'
    } else if (message.includes('Request failed with status code')) {
      message = '系统接口' + message.substr(message.length - 3) + '异常'
    }
    ElMessage({
      message: message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)
export default request

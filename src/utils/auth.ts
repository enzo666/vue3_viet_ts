
const TokenKey = `${import.meta.env.VUE_APP_ENV_CACHE_PREFIX}Token`

const ExpiresInKey = `${import.meta.env.VUE_APP_ENV_CACHE_PREFIX}In`

export function getToken() {
  return localStorage.getItem(TokenKey)
}

export function setToken(token: string) {
  return localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return localStorage.removeItem(TokenKey)
}

export function getExpiresIn() {
  return localStorage.getItem(ExpiresInKey) || -1
}

export function setExpiresIn(time: string) {
  return localStorage.setItem(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return localStorage.removeItem(ExpiresInKey)
}

import { defineStore } from 'pinia'
import { issuePublicKey, loginApi, getInfo, logout } from '@/api/user'
export const userStore = defineStore('user', {
  state: () => {
    return {
      token: '',
      info: {},
      region: {},
      rules: []
    }
  },

  actions: {
    // 登录
    async Login(params: any) {
      const res = await loginApi(params)
      this.setToken(res.data.access_token)
    },
    // 设置token
    setToken(token: string) {
      this.token = token
    },

    // 获取用户信息
    async GetInfo() {
      const res = await getInfo()
      return res.data[0]
    },

    // 退出系统
    LogOut() {
      return new Promise<void>((resolve, reject) => {
        logout()

        this.token = ''
        this.info = ''
        this.region = {}
        this.rules = []

        resolve()
      })
    }
  },
  getters: {},
  //开启数据持久化
  persist: true
})

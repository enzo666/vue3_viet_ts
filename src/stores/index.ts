import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { userStore } from './modules/user'

export function useStore() {
  return {
    user: userStore()
  }
}

import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import HomeView from '../views/home/index.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      TabbarShow: true //控制底部tab是否展示
    }
  },
  {
    path: '/home/backLog',
    name: 'backLog',
    component: () => import('../views/home/backLog.vue'),
    meta: {
      TabbarShow: true //控制底部tab是否展示
    }
  },
  {
    path: '/login/login',
    name: 'login',
    component: () => import('../views/login/login.vue'),
    
  }
]
const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: routes
})

export default router

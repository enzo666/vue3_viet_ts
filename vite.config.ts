import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { visualizer } from 'rollup-plugin-visualizer'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { VantResolver } from '@vant/auto-import-resolver'
import postCssPxToRem from 'postcss-pxtorem'
import vitePluginRequire from 'vite-plugin-require'
import { ProxyOptions } from 'vite' // 从 vite 获取类型
// https://vitejs.dev/config/
// export default defineConfig({

export default ({ mode }: { mode: string }) => {
  const env = loadEnv(mode, process.cwd())
  return defineConfig({
    plugins: [
      vue(),
      vueJsx(),
      visualizer({ open: true }),
      AutoImport({ imports: ['vue', 'vue-router'], dts: 'src/auto-imports.d.ts' }),
      Components({
        resolvers: [VantResolver()]
      }),
      vitePluginRequire({
        // @fileRegex RegExp
        // optional：default file processing rules are as follows
        // fileRegex:/(.jsx?|.tsx?|.vue)$/
        // Conversion mode. The default mode is import
        // importMetaUrl | import
        // importMetaUrl see https://vitejs.cn/guide/assets.html#new-url-url-import-meta-url
        // translateType: "importMetaUrl" | "import";
      })
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      proxy: {
        [env.VITE_APIPROXY]: {
          // 配置需要代理的路径 --> 这里的意思是代理http://localhost:80/api/后的所有路由
          target: env.VITE_APIPROXYURL, // 目标地址 --> 服务器地址
          changeOrigin: true, // 允许跨域
          ws: true, // 允许websocket代理
          // 重写路径 --> 作用与vue配置pathRewrite作用相同
          rewrite: (path) => path.replace(RegExp(`^${env.VITE_APIPROXY}`), ''),
          // rewrite: (path) => path.replace(RegExp(`^${env.VITE_APIPROXY}`), ''),
          //可查看代理之后的路径地址
          // bypass(req, res, options: ProxyOptions | undefined) {
          //   // 确保 options 的类型
          //   if (options) {
          //     const proxyUrl =
          //       new URL(options.rewrite(req.url) || '', options.target as string)?.href || ''
          //     console.log(proxyUrl)
          //     req.headers['x-req-proxyUrl'] = proxyUrl
          //     res.setHeader('x-res-proxyUrl', proxyUrl)
          //   } else {
          //     console.warn('Proxy options are undefined')
          //   }
          // }
        }
      }
    },
    css: {
      preprocessorOptions: {
        sass: {
          api: 'modern-compiler' // or "modern"
        }
        // scss: {
        //    additionalData:  '@use "@/assets/style/mixin.scss";'
        // }
      },

      postcss: {
        plugins: [
          postCssPxToRem({
            rootValue: 37.5, //换算基数，
            propList: ['*'], // 需要转换的属性，这里选择全部都进行转换
            selectorBlackList: ['.van'] //要忽略并保留为px的选择器，本项目我是用的vant ui框架，所以忽略他
          })
        ]
      }
    }
  })
}
